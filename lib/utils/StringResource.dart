class StringResource {
  static const String debitCard = "Debit Card";
  static const String expiry = "Expires";
  static const String holderName = "Hemavathi";
  static const String expDate = "03/21";
  static const String cardNum = "****  3211";
  static const String accBal = "₹ 3000";
  static const String pay = "Pay";
  static const String transfer = "Transfer";
  static const String buy = "Buy";
  static const String other = "Other";
  static const String rectrans = "Recent transactions";
}
