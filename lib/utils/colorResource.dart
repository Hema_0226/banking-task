import 'package:flutter/cupertino.dart';

class ColorResource {
  static const Color notificationOrange = Color(0xfff19b3b);
  static const Color cardColor = Color(0xfff0f5ff);
  static const Color expText = Color(0xffd2daeb);
  static const Color holderName = Color(0xffa9aeb7);
  static const Color recTransbg = Color(0xffeef8ff);
}
