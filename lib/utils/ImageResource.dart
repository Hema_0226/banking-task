class ImageResource {
  static const String menuIcon = "images/bar.png";
  static const String visaLogo = "images/visalogo.png";
  static const String pay = "images/barcode.png";
  static const String transfer = "images/transfer.png";
  static const String buy = "images/cart.png";
  static const String other = "images/other.png";
  static const String recIcon = "images/trans.png";
}
