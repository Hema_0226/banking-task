import 'package:banking_app/utils/ImageResource.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

// ignore: must_be_immutable
class RecentTransactionWidget extends StatefulWidget {
  String icon;
  String name;
  String date;
  String amt;
  RecentTransactionWidget(this.icon, this.name, this.date, this.amt);

  @override
  _RecentTransactionWidgetState createState() =>
      _RecentTransactionWidgetState();
}

class _RecentTransactionWidgetState extends State<RecentTransactionWidget> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: ListTile(
        leading: Container(
            height: 40,
            width: 40,
            decoration: BoxDecoration(
                gradient: LinearGradient(
                  colors: [Color(0xfff9feff), Color(0xffeef8ff)],
                  begin: Alignment.topRight,
                  end: Alignment.bottomRight,
                ),
                shape: BoxShape.circle,
                color: Colors.white,
                boxShadow: [
                  BoxShadow(blurRadius: 3, offset: Offset(0, 2)),
                ]),
            child: Image.asset(ImageResource.recIcon)),
        title: Text(widget.name,
            style: GoogleFonts.sourceSansPro(
                textStyle: TextStyle(
              fontSize: 14,
              fontWeight: FontWeight.w500,
              color: Colors.black,
            ))),
        subtitle: Text(widget.date,
            style: GoogleFonts.sourceSansPro(
                textStyle: TextStyle(
              fontSize: 12,
              fontWeight: FontWeight.w500,
            ))),
        trailing: Text(widget.amt,
            style: GoogleFonts.sourceSansPro(
                textStyle: TextStyle(
              fontSize: 16,
              fontWeight: FontWeight.w500,
            ))),
      ),
    );
  }
}
