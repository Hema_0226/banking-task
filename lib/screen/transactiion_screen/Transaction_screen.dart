import 'package:banking_app/utils/ImageResource.dart';
import 'package:banking_app/utils/StringResource.dart';
import 'package:banking_app/utils/colorResource.dart';
import 'package:banking_app/widgets/recent_transaction_widget.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class TransactionScreen extends StatefulWidget {
  const TransactionScreen({Key? key}) : super(key: key);

  @override
  _TransactionScreenState createState() => _TransactionScreenState();
}

class _TransactionScreenState extends State<TransactionScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        // backgroundColor: Colors.red,
        appBar: AppBar(
          backgroundColor: Color(0xfff9feff),
          elevation: 0,
          leading: Container(
              margin: EdgeInsets.only(left: 15, right: 15),
              child: Image.asset(ImageResource.menuIcon)),
          actions: [
            Container(
              margin: EdgeInsets.only(right: 15),
              height: 20,
              width: 20,
              child: Icon(
                Icons.notifications_none_rounded,
                color: ColorResource.notificationOrange,
              ),
            )
          ],
        ),
        body: Container(
          decoration: BoxDecoration(
              gradient: LinearGradient(
                  begin: Alignment.topLeft,
                  end: Alignment.bottomRight,
                  colors: [Color(0xfff9feff), ColorResource.cardColor])),
          child: SafeArea(
            child: Column(children: [
              Container(
                margin:
                    EdgeInsets.only(top: 15, left: 15, right: 15, bottom: 30),
                decoration: BoxDecoration(
                  boxShadow: [BoxShadow(blurRadius: 10, offset: Offset(0, 2))],
                  color: ColorResource.cardColor,
                  borderRadius: BorderRadius.circular(15),
                ),
                child: Column(
                  children: [
                    Container(
                      margin: EdgeInsets.only(
                          left: 15, right: 15, top: 15, bottom: 15),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(StringResource.debitCard,
                              style: GoogleFonts.sourceSansPro(
                                  textStyle: TextStyle(
                                fontSize: 14,
                                fontWeight: FontWeight.normal,
                                color: ColorResource.expText,
                              ))),
                          Text(StringResource.expiry,
                              style: GoogleFonts.sourceSansPro(
                                  textStyle: TextStyle(
                                fontSize: 14,
                                fontWeight: FontWeight.normal,
                                color: ColorResource.expText,
                              ))),
                        ],
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(left: 15, right: 15),
                      child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(StringResource.holderName,
                                style: GoogleFonts.sourceSansPro(
                                    textStyle: TextStyle(
                                  fontSize: 14,
                                  fontWeight: FontWeight.w600,
                                  color: ColorResource.holderName,
                                ))),
                            Text(StringResource.expDate,
                                style: GoogleFonts.sourceSansPro(
                                    textStyle: TextStyle(
                                  fontSize: 14,
                                  fontWeight: FontWeight.w600,
                                  color: ColorResource.holderName,
                                ))),
                          ]),
                    ),
                    Container(
                      margin: EdgeInsets.only(left: 15, top: 50),
                      child: Row(
                        children: [
                          Text(StringResource.cardNum,
                              style: GoogleFonts.sourceSansPro(
                                  textStyle: TextStyle(
                                fontSize: 14,
                                fontWeight: FontWeight.w600,
                                color: ColorResource.holderName,
                              ))),
                        ],
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(
                          left: 15, top: 10, right: 15, bottom: 15),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(StringResource.accBal,
                              style: GoogleFonts.sourceSansPro(
                                  textStyle: TextStyle(
                                fontSize: 24,
                                fontWeight: FontWeight.w800,
                                color: ColorResource.notificationOrange,
                              ))),
                          Container(
                              height: 40,
                              width: 50,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(15),
                                color: Colors.white,
                              ),
                              child: Image.asset(ImageResource.visaLogo)),
                        ],  
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(left: 30, right: 30),
                      decoration: BoxDecoration(
                          boxShadow: [
                            BoxShadow(blurRadius: 15, offset: Offset(0, 2))
                          ],
                          border: Border.all(
                            color: Color(0xffe3eefa),
                          ),
                          borderRadius: BorderRadius.only(
                              bottomLeft: Radius.circular(15),
                              bottomRight: Radius.circular(15))),
                    )
                  ],
                ),
              ),
              Expanded(
                flex: 1,
                child: ListView(scrollDirection: Axis.horizontal, children: [
                  Container(
                    child: Row(
                      children: [
                        Column(
                          children: [
                            Container(
                              margin: EdgeInsets.only(left: 15, right: 15),
                              height: 50,
                              width: 50,
                              decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                color: ColorResource.notificationOrange,
                              ),
                              child: Icon(
                                Icons.account_tree,
                                color: Colors.white,
                              ),
                            ),
                            Text("Pay"),
                          ],
                        ),
                        Column(
                          children: [
                            Container(
                              margin: EdgeInsets.only(left: 15, right: 15),
                              height: 50,
                              width: 50,
                              decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                color: ColorResource.notificationOrange,
                              ),
                              child: Icon(
                                Icons.account_balance_sharp,
                                color: Colors.white,
                              ),
                            ),
                            Text("Transfer"),
                          ],
                        ),
                        Column(
                          children: [
                            Container(
                                margin: EdgeInsets.only(left: 15, right: 15),
                                height: 50,
                                width: 50,
                                decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  color: ColorResource.notificationOrange,
                                ),
                                child: Icon(
                                  Icons.ac_unit,
                                  color: Colors.white,
                                )),
                            Text("Buy"),
                          ],
                        ),
                        Column(
                          children: [
                            Container(
                              margin: EdgeInsets.only(left: 15, right: 15),
                              height: 50,
                              width: 50,
                              decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                color: ColorResource.notificationOrange,
                              ),
                              child: Image.asset(ImageResource.other),
                            ),
                            Text("Other"),
                          ],
                        ),
                        Column(
                          children: [
                            Container(
                                margin: EdgeInsets.only(left: 15, right: 15),
                                height: 50,
                                width: 50,
                                decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  color: ColorResource.notificationOrange,
                                ),
                                child: Image.asset(ImageResource.other)),
                            Text("Recharge"),
                          ],
                        ),
                        Column(
                          children: [
                            Container(
                                margin: EdgeInsets.only(left: 15, right: 15),
                                height: 50,
                                width: 50,
                                decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  color: ColorResource.notificationOrange,
                                ),
                                child: Image.asset(ImageResource.other)),
                            Text("Loan"),
                          ],
                        ),
                      ],
                    ),
                  )
                ]),
              ),
              Expanded(
                flex: 3,
                child: Container(
                  margin: EdgeInsets.only(left: 15, right: 15),
                  padding: EdgeInsets.only(left: 10, right: 10, top: 20),
                  decoration: BoxDecoration(
                    boxShadow: [
                      BoxShadow(
                        blurRadius: 0,
                        offset: Offset(0, 1),
                      )
                    ],
                    color: Color(0xfff9feff),
                    borderRadius: BorderRadius.circular(15),
                  ),
                  child: Column(
                    children: [
                      Row(
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(left: 25),
                            child: Text(StringResource.rectrans,
                                style: GoogleFonts.sourceSansPro(
                                    textStyle: TextStyle(
                                  fontSize: 18,
                                  fontWeight: FontWeight.w800,
                                  color: Colors.black,
                                ))),
                          ),
                        ],
                      ),
                      RecentTransactionWidget(ImageResource.recIcon,
                          "HemaPriya", "26 Jun 2021", "₹ 300"),
                      RecentTransactionWidget(ImageResource.recIcon,
                          "Keerthana", "1 JUl 2021", "₹ 1000"),
                      RecentTransactionWidget(ImageResource.recIcon, "Hari",
                          "22 Jun 2021", "₹ 1232"),
                      // RecentTransactionWidget(ImageResource.recIcon, "Haritha",
                      //     "22 Jun 2021", "₹ 232"),
                      // RecentTransactionWidget(ImageResource.recIcon, "Nainika",
                      //     "22 Jun 2021", "₹ 132"),
                      // RecentTransactionWidget(ImageResource.recIcon, "yamuna",
                      //     "22 Jun 2021", "₹ 122"),
                    ],
                  ),
                ),
              )
            ]),
          ),
        ));
  }

  Widget _selectedItem() {
    return Container();
  }
}
