import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

part 'transactionscreen_event.dart';
part 'transactionscreen_state.dart';

class TransactionscreenBloc
    extends Bloc<TransactionscreenEvent, TransactionscreenState> {
  TransactionscreenBloc() : super(TransactionscreenInitial());

  @override
  Stream<TransactionscreenState> mapEventToState(
    TransactionscreenEvent event,
  ) async* {}
}
